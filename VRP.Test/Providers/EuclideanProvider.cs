﻿using System;
using System.Collections.Generic;
using System.Text;
using VRP.Core;
using VRP.Core.Locations;
using VRP.Providers;

namespace VRP.Test.Providers
{
    class EuclideanProvider : IDistancesProvider
    {
        public double GetDistance(Location from, Location to)
        {
            return Math.Sqrt((from.X - to.X) * (from.X - to.X) + (from.Y - to.Y) * (from.Y - to.Y));
        }
    }
}
