﻿using System;
using System.Collections.Generic;
using System.Text;
using VRP.Core;
using VRP.Core.Locations;
using VRP.Providers;

namespace VRP.Test.Providers
{
    class VRPPFourByFourMatrixProvider : IDistancesProvider, ILocationsProvider
    {
        static Location depot = new SimpleLocation(0L, 0, 0, 0);
        static Location request1 = new SimpleLocation(1L, 1, 0, 1);
        static Location request2 = new SimpleLocation(2L, 1, 1, 1);
        static Location request3 = new SimpleLocation(3L, 0, 1, 1);

        static Dictionary<long, Dictionary<long, double>> distances = new Dictionary<long, Dictionary<long, double>>();

        static VRPPFourByFourMatrixProvider()
        {
            distances.Add(0L, new Dictionary<long, double>());
            distances.Add(1L, new Dictionary<long, double>());
            distances.Add(2L, new Dictionary<long, double>());
            distances.Add(3L, new Dictionary<long, double>());
            distances[0L].Add(0L, 0.0);
            distances[0L].Add(1L, 1.0);
            distances[0L].Add(2L, Math.Sqrt(2));
            distances[0L].Add(3L, 1.01);
            distances[1L].Add(0L, 1.01);
            distances[1L].Add(1L, 0.0);
            distances[1L].Add(2L, 1.0);
            distances[1L].Add(3L, Math.Sqrt(2));
            distances[2L].Add(0L, Math.Sqrt(2));
            distances[2L].Add(1L, 1.01);
            distances[2L].Add(2L, 0.0);
            distances[2L].Add(3L, 1.0);
            distances[3L].Add(0L, 1.0);
            distances[3L].Add(1L, Math.Sqrt(2));
            distances[3L].Add(2L, 1.01);
            distances[3L].Add(3L, 0.0);
        }

        public Location GetDepot()
        {
            return depot.Copy();
        }

        public double GetDistance(Location from, Location to)
        {
            return distances[from.Id][to.Id];
        }

        public List<Location> GetLocations()
        {
            List<Location> locations = new List<Location>();
            locations.Add(request1.Copy());
            locations.Add(request2.Copy());
            locations.Add(request3.Copy());
            return locations;
        }
    }
}
