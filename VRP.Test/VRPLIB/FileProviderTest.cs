﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using VRP.Providers;
using VRP.Providers.VRPLIB;
using VRP.Core.TSPAlgorithms;
using System.IO;
using System.Linq;
using VRP.Core.Locations;

namespace VRP.Test.VRPLIB
{
    [TestClass]
    public class FileProviderTest
    {
        [TestMethod]
        public void TestReadCVRPLibFile()
        {
            var provider = new CVRPLibProvider("Instances/X-n101-k25.vrp", new NoFurtherOptRouteFactory());

            IVehiclesProvider vehiclesProvider = provider;
            var vehicles = vehiclesProvider.GetVehicles();
            Assert.AreEqual(101, vehicles.Count);
            Assert.AreEqual(206, vehicles[0].Capacity);
            Assert.AreEqual(206, vehicles[100].Capacity);

            ILocationsProvider locationsProvider = provider;
            var locations = locationsProvider.GetLocations();
            var depot = locationsProvider.GetDepot();
            Assert.AreEqual(100, locations.Count);
            Assert.AreEqual(365, depot.X);
            Assert.AreEqual(689, depot.Y);
            Assert.AreEqual(0, depot.Size);
            Assert.AreEqual(146, locations[0].X);
            Assert.AreEqual(180, locations[0].Y);
            Assert.AreEqual(38, locations[0].Size);
            Assert.AreEqual(615, locations[99].X);
            Assert.AreEqual(750, locations[99].Y);
            Assert.AreEqual(35, locations[99].Size);
        }

        [TestMethod]
        public void TestReadBakalaFile()
        {
            var provider = new ClassifiedLocationsAndExplicitMatrixExtCVRPLibProvider("Instances/WarsawOld/Map401Adv.vrp"
                , new NoFurtherOptRouteFactory());

            IVehiclesProvider vehiclesProvider = provider;
            var vehicles = vehiclesProvider.GetVehicles();
            Assert.AreEqual(401, vehicles.Count);
            Assert.AreEqual(10500, vehicles[0].Capacity);
            Assert.AreEqual(10500, vehicles[400].Capacity);

            ILocationsProvider locationsProvider = provider;
            var locations = locationsProvider.GetLocations();
            var depot = locationsProvider.GetDepot();
            Assert.AreEqual(400, locations.Count);
            Assert.AreEqual(20.97036, depot.X);
            Assert.AreEqual(52.254429, depot.Y);
            Assert.AreEqual(0, depot.Size);
            Assert.AreEqual(20.9398822964784, locations[0].X);
            Assert.AreEqual(52.2408056794241, locations[0].Y);
            Assert.AreEqual(74, locations[0].Size);
            Assert.AreEqual(20.945500608316, locations[399].X);
            Assert.AreEqual(52.2393348410571, locations[399].Y);
            Assert.AreEqual(50, locations[399].Size);

        }

        [TestMethod]
        public void TestReadUpdatesOneBasedIndexBakalaFile()
        {
            var provider = new ClassifiedLocationsAndExplicitMatrixExtCVRPLibProvider(
                "Instances/Warsaw/W-n201-k2-c10500.vrp"
                , new NoFurtherOptRouteFactory());

            IVehiclesProvider vehiclesProvider = provider;
            var vehicles = vehiclesProvider.GetVehicles();
            Assert.AreEqual(201, vehicles.Count);
            Assert.AreEqual(10500, vehicles[0].Capacity);
            Assert.AreEqual(10500, vehicles[200].Capacity);

            ILocationsProvider locationsProvider = provider;
            var locations = locationsProvider.GetLocations();
            var depot = locationsProvider.GetDepot();
            Assert.AreEqual(200, locations.Count);
            Assert.AreEqual(20.97036, depot.X);
            Assert.AreEqual(52.254429, depot.Y);
            Assert.AreEqual(0, depot.Size);
            Assert.AreEqual(20.9739847332406, locations[0].X);
            Assert.AreEqual(52.2187479154555, locations[0].Y);
            Assert.AreEqual(142, locations[0].Size);
            Assert.AreEqual(20.9814420484189, locations[199].X);
            Assert.AreEqual(52.220293978973, locations[199].Y);
            Assert.AreEqual(10, locations[199].Size);

            IDistancesProvider distancesProvider = provider;
            Assert.AreEqual(5731, provider.GetDistance(depot, locations[0]));
            Assert.AreEqual(5023, provider.GetDistance(depot, locations[199]));

        }


        [TestMethod]
        public void TestCompressingProvider()
        {
            var standardProvider = ProviderFactory.GetProviderForFile(new FileInfo("Instances/Warsaw/W-n201-k2-c10500.vrp"), new TwoOptRouteFactory(), false);
            var groupedProvider = ProviderFactory.GetProviderForFile(new FileInfo("Instances/Warsaw/W-n201-k2-c10500.vrp"), new TwoOptRouteFactory(), true);
            Assert.IsNotNull(groupedProvider);

            var countGroupedLocations = groupedProvider.GetLocations().Count;
            var countNormalLocations = standardProvider.GetLocations().Count;
            var countLocationsInGroups = groupedProvider.GetLocations().Sum(gloc => (gloc as GroupedLocation).LocationsCount);

            Assert.IsTrue(countGroupedLocations <= countNormalLocations);
            Assert.AreEqual(countNormalLocations, countLocationsInGroups);
            //This test is for regression purpose - I am not sure if this is a true value
            //I only want to know when it changes
            Assert.AreEqual(93, countGroupedLocations);

            var sumGroupedLocations = groupedProvider.GetLocations().Sum(loc => loc.Size);
            var sumNormalLocations = standardProvider.GetLocations().Sum(loc => loc.Size);

            Assert.AreEqual(sumNormalLocations, sumGroupedLocations);

        }
    }
}
