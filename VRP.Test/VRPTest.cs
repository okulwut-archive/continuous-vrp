﻿using ContVRP;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ParticleSwarmOptimization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using VRP.Core.TSPAlgorithms;
using VRP.Core.VRPAlgorithms;
using VRP.Providers.VRPLIB;

namespace VRP.Test
{
    [TestClass]
    public class VRPTest
    {
        [TestMethod]
        public void OptimizeTest()
        {
            var provider = new CVRPLibProvider("Instances/X-n101-k25.vrp", new NoFurtherOptRouteFactory());
            VRPAlgorithm vrp = new GreedyVRP(provider, provider, provider);
            vrp.Optimize();
            var solution = vrp.GetSolution();
            Assert.AreEqual(38822, vrp.TotalLength);
            var locations = provider.GetLocations();
            foreach (var location in locations)
            {
                Assert.IsTrue(solution.Any(vhcl =>
                    vhcl.Schedule.Any(loc => loc.Id == location.Id)));
            }
            foreach (var location in locations)
            {
                Assert.AreEqual(1, solution.Sum(vhcl =>
                    vhcl.Schedule.Where(loc => loc.Id == location.Id)
                    .Count()));
            }

            vrp = new TreeClusteringVRP(provider, provider, provider);
            vrp.Optimize();
            solution = vrp.GetSolution();
            Assert.AreEqual(33354, vrp.TotalLength);
            locations = provider.GetLocations();
            foreach (var location in locations)
            {
                Assert.IsTrue(solution.Any(vhcl =>
                    vhcl.Schedule.Any(loc => loc.Id == location.Id)));
            }
            foreach (var location in locations)
            {
                Assert.AreEqual(1, solution.Sum(vhcl =>
                    vhcl.Schedule.Where(loc => loc.Id == location.Id)
                    .Count()));
            }
        }

        [TestMethod]
        public void OptimizeGroupedProblems()
        {
            var standardProvider = ProviderFactory.GetProviderForFile(new FileInfo("Instances/Warsaw/W-n201-k2-c10500.vrp"), new TwoOptRouteFactory(), false);
            var groupedProvider = ProviderFactory.GetProviderForFile(new FileInfo("Instances/Warsaw/W-n201-k2-c10500.vrp"), new TwoOptRouteFactory(), true);

            VRPAlgorithm greedy = new GreedyVRP(groupedProvider, groupedProvider, groupedProvider);
            greedy.Optimize();
            var greedySolution = greedy.GetSolution();
            Assert.AreEqual(standardProvider.GetLocations().Count, greedySolution.SelectMany(veh => veh.Schedule).Count(loc => loc.Size > 0));

            VRPAlgorithm tree = new TreeClusteringVRP(groupedProvider, groupedProvider, groupedProvider);
            tree.Optimize();
            var treeSolution = tree.GetSolution();
            Assert.AreEqual(standardProvider.GetLocations().Count, treeSolution.SelectMany(veh => veh.Schedule).Count(loc => loc.Size > 0));

            BaseContinuousVRP contVrp = new ContinuousVRPWithExplicitCapacityClustering(
                10,
                10,
                1,
                new List<VRPAlgorithm>()
                {
                    new GreedyVRP(groupedProvider, groupedProvider, groupedProvider),
                    new TreeClusteringVRP(groupedProvider, groupedProvider, groupedProvider)
                },
                groupedProvider);
            contVrp.Optimize();
            var contVrpSolution = contVrp.GetSolution();
            Assert.AreEqual(standardProvider.GetLocations().Count, contVrpSolution.SelectMany(veh => veh.Schedule).Count(loc => loc.Size > 0));


        }

        [TestMethod]
        public void ContinuousOptimizationTest()
        {
            var provider = new CVRPLibProvider("Instances/X-n101-k25.vrp", new NoFurtherOptRouteFactory());
            BaseContinuousVRP vrp = new ContinuousVRPWithExplicitCapacityClustering(
                10,
                10,
                1,
                new List<VRP.Core.VRPAlgorithms.VRPAlgorithm>()
                {
                    new GreedyVRP(provider, provider, provider),
                    new TreeClusteringVRP(provider, provider, provider)
                },
                provider);

            var optimAlgorithm = new PSOAlgorithm(
                function: vrp,
                swarmSize: 10,
                radius: vrp.InitializationRadius,
                neighbourhood: 0.5,
                inventorsRatio: 0.5);
            for (int i = 0; i < 10 / 2; ++i)
            {
                optimAlgorithm.Step();
            }
            double expected = vrp.Value(optimAlgorithm.Best);
            double[] oldBest = optimAlgorithm.Best.ToArray();
            double actual = vrp.Value(oldBest);
            Assert.AreEqual(expected, actual);
            vrp.FreezeLocations();
            optimAlgorithm.ReloadState(vrp);
            for (int i = 0; i < 10 / 2; ++i)
            {
                optimAlgorithm.Step();
            }
            vrp.DefrostLocations();
            actual = vrp.Value(oldBest);
            Assert.AreEqual(expected, actual);
        }
    }
}
