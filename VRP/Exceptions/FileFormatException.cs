﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VRP.Exceptions
{
    public class FileFormatException : Exception
    {
        public FileFormatException()
        {
        }

        public FileFormatException(string message) : base(message)
        {
        }

        public FileFormatException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
