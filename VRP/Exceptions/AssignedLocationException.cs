﻿using System;

namespace VRP.Exceptions
{
    public class AssignedLocationException : Exception
    {
        public AssignedLocationException()
        {
        }

        public AssignedLocationException(string message) : base(message)
        {
        }

        public AssignedLocationException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}