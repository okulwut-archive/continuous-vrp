﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VRP.Exceptions
{
    public class NoSolutionException : Exception
    {
        public NoSolutionException()
        {
        }

        public NoSolutionException(string message) : base(message)
        {
        }

        public NoSolutionException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
