﻿using System;

namespace VRP.Exceptions
{
    public class RouteEndpointException : Exception
    {
        public RouteEndpointException()
        {
        }

        public RouteEndpointException(string message) : base(message)
        {
        }

        public RouteEndpointException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}