﻿using System;
using System.Collections.Generic;
using System.Text;
using VRP.Core.Locations;

namespace VRP.Providers
{
    public interface IClassifiedLocationsProvider: ILocationsProvider
    {
        List<ClassifiedLocation> GetClassifiedLocations();
    }
}
