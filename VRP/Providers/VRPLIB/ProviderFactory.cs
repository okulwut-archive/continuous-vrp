﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using VRP.Core;
using VRP.Core.TSPAlgorithms;
using VRP.Exceptions;

namespace VRP.Providers.VRPLIB
{
    public class ProviderFactory
    {
        public static IAllDataProvider GetProviderForFile(FileInfo fileInfo, RouteFactory routeFactory, bool applyCompression)
        {
            IAllDataProvider provider = null;
            if (applyCompression)
            {
                try
                {
                    var classifiedProvider = new ClassifiedLocationsAndExplicitMatrixExtCVRPLibProvider(fileInfo.FullName, new TwoOptRouteFactory());
                    return new GroupingProvider(classifiedProvider, classifiedProvider, classifiedProvider);
                }
                catch (FileFormatException)
                {
                    return null;
                }
            }
            try
            {
                provider = new CVRPLibProvider(fileInfo.FullName, routeFactory);
            }
            catch (FileFormatException)
            {
                try
                {
                    provider = new ClassifiedLocationsAndExplicitMatrixExtCVRPLibProvider(fileInfo.FullName, routeFactory);
                }
                catch (FileFormatException)
                {
                    return null;
                }
            }

            return provider;

        }
    }
}
