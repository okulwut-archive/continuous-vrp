﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using VRP.Core;
using VRP.Core.Locations;
using VRP.Core.TSPAlgorithms;
using VRP.Exceptions;

namespace VRP.Providers.VRPLIB
{
    public class ClassifiedLocationsAndExplicitMatrixExtCVRPLibProvider : BaseFileProvider, IClassifiedLocationsProvider, IDistancesAndHopsProvider
    {
        const string EXPLICIT = "EXPLICIT";
        const string EDGE_WEIGHT_FORMAT = "EDGE_WEIGHT_FORMAT";
        const string ADJ = "ADJ";
        const string EDGE_WEIGHT_SECTION = "EDGE_WEIGHT_SECTION";
        const string LOCATION_GROUP_SECTION = "LOCATION_GROUP_SECTION";
        private double[][] edgesDistance;
        private int[][] hopsDistance;
        private List<ClassifiedLocation> classifiedLocations;

        protected override int XCoordinate => 2;
        protected override int YCoordinate => 1;


        public ClassifiedLocationsAndExplicitMatrixExtCVRPLibProvider(string filename, RouteFactory routeFactory)
            : base(filename, routeFactory)
        {
        }

        public override double GetDistance(Location from, Location to)
        {
            if (from.Id == to.Id)
                return 0;
            return edgesDistance[(int)from.Id][(int)to.Id];
        }

        public int GetHops(Location from, Location to)
        {
            if (from.Id == to.Id)
                return 0;
            return hopsDistance[(int)from.Id][(int)to.Id];
        }

        public List<ClassifiedLocation> GetClassifiedLocations()
        {
            return classifiedLocations
                .Select(cloc => cloc.Copy() as ClassifiedLocation)
                .ToList();
        }

        protected override void CheckEdgeType(string[] edgeInfo)
        {
            if (edgeInfo[1] != EXPLICIT)
                throw new FileFormatException("Unsupported edge type " + edgeInfo[1]);
        }

        protected override int CheckingSections(string[] fileLines, int i)
        {
            i = base.CheckingSections(fileLines, i);
            if (fileLines[i].TrimStart().StartsWith(EDGE_WEIGHT_FORMAT))
            {
                CheckEdgeWeighType(fileLines, i);
            }
            if (fileLines[i].TrimStart().StartsWith(EDGE_WEIGHT_SECTION))
            {
                i = GettingEdges(fileLines, i);
            }
            if (fileLines[i].TrimStart().StartsWith(LOCATION_GROUP_SECTION))
            {
                i = SettingLocationGroups(fileLines, i);
            }
            return i;
        }

        private int SettingLocationGroups(string[] fileLines, int i)
        {
            var locations = GetLocations();
            classifiedLocations = new List<ClassifiedLocation>();
            i++;
            while (fileLines[i].Split(new char[] { DATA_SEPARATOR, ID_DATA_SEPARTOR },
                StringSplitOptions.RemoveEmptyEntries).Length == 3)
            {
                string[] data = fileLines[i].Split(ID_DATA_SEPARTOR, DATA_SEPARATOR);
                if (data.Length == 3)
                {
                    int id = int.Parse(data[0]) - 1;
                    int streetId = int.Parse(data[1]);
                    //int streetPartId = int.Parse(data[2]);
                    var location = locations.FirstOrDefault(loc => loc.Id == id);
                    if (location != null)
                    {
                        classifiedLocations.Add(new ClassifiedLocation(streetId, location.Id, location.X, location.Y, location.Size));
                    }
                }
                i++;
            }
            i--;
            return i;
        }

        protected virtual void CheckEdgeWeighType(string[] fileLines, int i)
        {
            string[] edgeInfo = GetProperty(fileLines, i);
            if (edgeInfo[1] != ADJ)
                throw new FileFormatException("Unsupported edge type " + edgeInfo[1]);
        }

        private int GettingEdges(string[] fileLines, int i)
        {
            int initialI = i;
            try
            {
                i = InnerGettingEdges(fileLines, initialI, 1);
            }
            catch (IndexOutOfRangeException)
            {
                //For backward compatibility
                i = InnerGettingEdges(fileLines, initialI, 0);
            }
            return i;
        }

        private int InnerGettingEdges(string[] fileLines, int i, int offset)
        {
            edgesDistance = new double[clientCount][];
            hopsDistance = new int[clientCount][];
            i++;
            while (fileLines[i].Split(new char[] { DATA_SEPARATOR, ID_DATA_SEPARTOR },
                StringSplitOptions.RemoveEmptyEntries).Length == 4)
            {
                string[] data = fileLines[i].Split(ID_DATA_SEPARTOR, DATA_SEPARATOR);
                if (data.Length == 4)
                {
                    int idFrom = int.Parse(data[0]) - offset;
                    int idTo = int.Parse(data[1]) - offset;
                    if (edgesDistance[idFrom] == null)
                    {
                        edgesDistance[idFrom] = new double[clientCount];
                        hopsDistance[idFrom] = new int[clientCount];
                    }
                    edgesDistance[idFrom][idTo] = double.Parse(data[2], CultureInfo.InvariantCulture);
                    hopsDistance[idFrom][idTo] = int.Parse(data[3]);
                }
                i++;
            }
            i--;
            return i;
        }
    }
}
