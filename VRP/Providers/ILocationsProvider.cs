﻿using System;
using System.Collections.Generic;
using System.Text;
using VRP.Core;
using VRP.Core.Locations;

namespace VRP.Providers
{
    public interface ILocationsProvider
    {
        List<Location> GetLocations();

        Location GetDepot();
    }
}
