﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VRP.Providers
{
    public interface IAllDataProvider: ILocationsProvider, IVehiclesProvider, IDistancesProvider
    {
    }
}
