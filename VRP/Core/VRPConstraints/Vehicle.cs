﻿using System;
using System.Collections.Generic;
using VRP.Core.Locations;

namespace VRP.Core.VRPConstraints
{
    public abstract class Vehicle
    {
        public double Capacity { get; private set; }
        public double TotalLength { get; protected set; }
        public double TotalSize { get; protected set; }
        public abstract IReadOnlyCollection<Location> Schedule { get; }

        public Vehicle(double capacity)
        {
            this.Capacity = capacity;
        }

        public abstract bool Insert(Location location);
        public abstract bool Add(Location location);
        public abstract bool Remove(Location location);

        public abstract double ComputeInsertionCost(Location location);

        public abstract void Optimize();
    }
}
