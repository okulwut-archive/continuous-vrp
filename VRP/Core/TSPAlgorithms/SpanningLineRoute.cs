﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRP.Providers;
using VRP.Utils.Clustering;
using VRP.Exceptions;
using VRP.Core.Locations;

namespace VRP.Core.TSPAlgorithms
{
    public class SpanningLineRoute : TwoOptRoute
    {
        public SpanningLineRoute(Location startPoint, Location endPoint, IDistancesProvider distancesProvider) : base(startPoint, endPoint, distancesProvider)
        {
        }

        public override void Optimize()
        {
            if (locations.Count < 3)
                return;
            List<ClusteredLocation> clusteredLocations = new List<ClusteredLocation>();
            this.locations.ForEach(
                loc => clusteredLocations.Add(new ClusteredLocation(loc)));
            var edges = GraphMSTClusterer.ComputeOrderedEdges(distancesProvider, clusteredLocations, clusteredLocations);
            var clusters = new GraphMSTClustererWithDirection(double.MaxValue, clusteredLocations, edges);
            this.locations.Clear();
            this.InitializeEmptyRoute(clusteredLocations.First().Location, clusteredLocations.Last().Location);
            //throw new NotImplementedException();
            Stack<IList<Edge>> forksStock = new Stack<IList<Edge>>();
            var currentLocation = clusteredLocations.First();
            while (currentLocation != clusteredLocations.Last())
            {
                IList<Edge> edgesFork = clusters.EdgesOutput
                    .Where(e => e.locationFrom.Id == currentLocation.Id)
                    .OrderByDescending(e => distancesProvider.GetDistance(e.locationTo.Location, clusteredLocations.Last().Location))
                    .ToList();
                if (edgesFork.Any())
                {
                    currentLocation = ProcessEdgeStock(forksStock, edgesFork);
                }
                else if (forksStock.Any())
                {
                    edgesFork = forksStock.Pop();
                    currentLocation = ProcessEdgeStock(forksStock, edgesFork);
                }
                else if (currentLocation != clusteredLocations.Last())
                {
                    throw new NoSolutionException("Spanning graph disjoint");
                }
            }
            base.Optimize();
        }

        private ClusteredLocation ProcessEdgeStock(Stack<IList<Edge>> forksStock, IList<Edge> edgesFork)
        {
            ClusteredLocation currentLocation;
            var nextLocation = edgesFork.First().locationTo.Location;
            double cost = ComputeInsertionCost(nextLocation, locations.Count - 1);
            Insert(nextLocation, cost, locations.Count - 1);
            currentLocation = edgesFork.First().locationTo;
            edgesFork.RemoveAt(0);
            if (edgesFork.Any())
            {
                forksStock.Push(edgesFork);
            }

            return currentLocation;
        }
    }
}
