﻿using System;
using System.Collections.Generic;
using System.Text;
using VRP.Core.Locations;
using VRP.Providers;

namespace VRP.Core.TSPAlgorithms
{
    public abstract class Route
    {
        protected List<Location> locations;
        protected IDistancesProvider distancesProvider;
        public double Length { get; protected set; }
        public double TotalSize { get; protected set; }
        public IReadOnlyCollection<Location> Schedule { get { return locations.AsReadOnly(); } }

        protected Route(IDistancesProvider distancesProvider)
        {
            this.distancesProvider = distancesProvider;
            this.locations = new List<Location>();
        }

        protected double ComputeInsertionCost(GroupedLocation newLocation, int i)
        {
            throw new NotImplementedException();
        }
        protected double ComputeInsertionCost(Location newLocation, int i)
        {
            double testAdditionalDistance = 0.0;
            if (i > 0)
            {
                double fromOldToNew = distancesProvider.GetDistance(locations[i - 1], newLocation);
                testAdditionalDistance += fromOldToNew;
            }
            if (i < locations.Count)
            {
                double fromNewToOld = distancesProvider.GetDistance(newLocation, locations[i]);
                testAdditionalDistance += fromNewToOld;
            }
            if (i > 0 && i < locations.Count)
            {
                double fromOldToOld = distancesProvider.GetDistance(locations[i - 1], locations[i]);
                testAdditionalDistance -= fromOldToOld;
            }
            return testAdditionalDistance;
        }

        protected void Insert(GroupedLocation newLocation, double insertionCost, int index)
        {
            throw new NotImplementedException();
        }

        protected void Insert(Location newLocation, double insertionCost, int index)
        {
            locations.Insert(index, newLocation);
            Length += insertionCost;
            TotalSize += newLocation.Size;
            if (index == 0)
            {
                locations[index].Rank = 0;
                ++index;
            }
            for (int i = index; i < locations.Count; ++i)
            {
                locations[i].Rank = locations[i - 1].Rank + 1;
            }
        }

        protected double FindMinimalCostInsert(Location newLocation, out int index, int lowerInclusiveIndexBound, int upperExclusiveIndexBound)
        {
            index = -1;
            double additionalDistance = double.PositiveInfinity;
            for (int i = lowerInclusiveIndexBound; i < upperExclusiveIndexBound; ++i)
            {
                double testAdditionalDistance = ComputeInsertionCost(newLocation, i);
                if (testAdditionalDistance < additionalDistance)
                {
                    additionalDistance = testAdditionalDistance;
                    index = i;
                }
            }
            return additionalDistance;
        }

    }
}
