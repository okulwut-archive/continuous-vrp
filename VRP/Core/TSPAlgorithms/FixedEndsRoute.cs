﻿using System;
using System.Collections.Generic;
using System.Text;
using VRP.Core.Locations;
using VRP.Exceptions;
using VRP.Providers;

namespace VRP.Core.TSPAlgorithms
{
    public abstract class FixedEndsRoute: Route
    {
        public FixedEndsRoute(Location startPoint, Location endPoint, IDistancesProvider distancesProvider)
            : base(distancesProvider)
        {
            InitializeEmptyRoute(startPoint, endPoint);
        }

        protected void InitializeEmptyRoute(Location startPoint, Location endPoint)
        {
            if (locations.Count > 0)
                throw new RouteEndpointException("Initialzing non-empty route");
            var newObjectStartPoint = startPoint.Copy();
            var newObjectEndPoint = endPoint.Copy();
            double startInsertionCost = ComputeInsertionCost(newObjectStartPoint, 0);
            Insert(newObjectStartPoint, startInsertionCost, 0);
            double endInsertionCost = ComputeInsertionCost(newObjectEndPoint, 1);
            Insert(newObjectEndPoint, endInsertionCost, 1);
        }

        public bool TryInsert(Location newLocation, out double insertionCost)
        {
            insertionCost = ComputeLowestCostInsertionPlace(newLocation, out int index);
            if (index == 0 || index > locations.Count - 1)
                throw new RouteEndpointException("Route endpoint might have been changed");
            if (index > 0)
            {
                if (newLocation.Route != null)
                    throw new AssignedLocationException("Trying to insert already assigned location");
                insertionCost = newLocation.InsertToRoute(this, index);
                return true;
            }
            else
            {
                return false;
            }
        }

        internal double Insert(Location newLocation, int index)
        {
            var insertionCost = ComputeInsertionCost(newLocation, index);
            Insert(newLocation, insertionCost, index);
            return insertionCost;
        }


        public void Add(Location newLocation, out double insertionCost)
        {
            insertionCost = ComputeInsertionCost(newLocation, locations.Count - 1);
            Insert(newLocation, insertionCost, locations.Count - 1);
        }


        public double ComputeLowestCostInsertionPlace(Location newLocation, out int index)
        {
            int lowerInclusiveIndexBound = 1;
            int upperExclusiveIndexBound = locations.Count;
            return FindMinimalCostInsert(
                newLocation,
                out index,
                lowerInclusiveIndexBound,
                upperExclusiveIndexBound);
        }

        public bool Remove(Location location, out double removalCost)
        {
            int index = locations.IndexOf(location);
            removalCost = 0.0;
            if (index == 0 || index > locations.Count - 1)
                throw new RouteEndpointException("Cannot remove route endpoint");
            if (index < 0)
                return false;
            location.ClearRoute();
            double fromRemainingToRemoved = distancesProvider.GetDistance(locations[index - 1], location);
            double fromRemovedToRemaining = distancesProvider.GetDistance(location, locations[index + 1]);
            double fromRemainingToRemaining = distancesProvider.GetDistance(locations[index-1], locations[index + 1]);
            removalCost = fromRemainingToRemaining - fromRemainingToRemoved - fromRemovedToRemaining;
            Length += removalCost;
            TotalSize -= location.Size;
            locations.RemoveAt(index);
            return true;
        }

        abstract public void Optimize();

    }
}
