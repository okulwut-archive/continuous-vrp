﻿using System;
using System.Collections.Generic;
using System.Text;
using VRP.Providers;
using VRP.Core.Locations;

namespace VRP.Core.TSPAlgorithms
{
    public abstract class RouteFactory
    {
        public abstract FixedEndsRoute CreateRoute(Location startPoint, Location endPoint, IDistancesProvider distancesProvider);
    }
}
