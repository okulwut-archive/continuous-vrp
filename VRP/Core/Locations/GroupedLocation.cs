﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRP.Core.TSPAlgorithms;
using VRP.Exceptions;
using VRP.Providers;

namespace VRP.Core.Locations
{
    public class GroupedLocation : Location, IClassifiedLocation
    {
        private OpenEndsRoute route;
        private IDistancesAndHopsProvider hopsProvider;
        private readonly List<ClassifiedLocation> classifiedLocations;
        public int LocationsCount => classifiedLocations.Count;

        public long ClassId { get; private set; }

        public GroupedLocation(ClassifiedLocation location, IDistancesAndHopsProvider hopsProvider) : base( location.Id, location.X, location.Y, 0.0)
        {
            this.ClassId = location.ClassId;
            this.hopsProvider = hopsProvider;
            this.classifiedLocations = new List<ClassifiedLocation>();
            route = new OpenEndsRoute(hopsProvider);
            Insert(location);
        }

        private GroupedLocation(IList<ClassifiedLocation> locations, IDistancesAndHopsProvider hopsProvider)
            : base(locations[locations.Count / 2].Id, locations[locations.Count / 2].X, locations[locations.Count / 2].Y, 0.0)
        {
            this.ClassId = locations[locations.Count / 2].ClassId;
            this.hopsProvider = hopsProvider;
            this.classifiedLocations = new List<ClassifiedLocation>();
            route = new OpenEndsRoute(hopsProvider);
            foreach (var location in locations)
            {
                Insert(location);
            }
        }

        public void Insert(ClassifiedLocation location)
        {
            if (ClassId != location.ClassId)
                throw new AssignedLocationException("Locations belong to different classes");
            classifiedLocations.Insert(route.Insert(location), location);
            Size += location.Size;
            var midpointLocation = classifiedLocations[classifiedLocations.Count / 2];
            this.Id = midpointLocation.Id;
            this.X = midpointLocation.X;
            this.Y = midpointLocation.Y;
        }

        public override Location Copy()
        {
            return new GroupedLocation(classifiedLocations, hopsProvider);
        }

        public List<GroupedLocation> Divide(double capacity)
        {
            double sum = 0.0;
            var dividedPaths = new List<GroupedLocation>();
            var currentAccumulator = new List<ClassifiedLocation>();
            for (int i = 0; i < classifiedLocations.Count; ++i)
            {
                currentAccumulator.Add(classifiedLocations[i]);
                sum += classifiedLocations[i].Size;
                if ((i == classifiedLocations.Count - 1) || (sum + classifiedLocations[i + 1].Size > capacity))
                {
                    dividedPaths.Add(
                        new GroupedLocation(
                            currentAccumulator,
                            hopsProvider));
                    currentAccumulator.Clear();
                    sum = 0.0;
                }
            }
            return dividedPaths;
        }

        internal override double InsertToRoute(FixedEndsRoute fixedEndsRoute, int index)
        {
            this.Route = fixedEndsRoute;
            double cost = 0.0;
            foreach (var location in classifiedLocations)
            {
                fixedEndsRoute.ComputeLowestCostInsertionPlace(location, out int betterIndex);
                cost += location.InsertToRoute(fixedEndsRoute, betterIndex);
            }
            return cost;
        }

        public override void ClearRoute()
        {
            this.Route = null;
            foreach (var location in classifiedLocations)
            {
                location.ClearRoute();
            }
        }
    }
}
