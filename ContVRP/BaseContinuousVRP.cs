﻿using ParticleSwarmOptimization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRP.Core;
using VRP.Core.Locations;
using VRP.Core.TSPAlgorithms;
using VRP.Core.VRPAlgorithms;
using VRP.Core.VRPConstraints;
using VRP.Providers;

namespace ContVRP
{
    public abstract class BaseContinuousVRP : GreedyVRP, IFunction
    {
        protected class ContinuousCluster
        {
            public double X { get; set; }
            public double Y { get; set; }
            private FixedEndsRoute ClusterTSPRoute { get; set; }

            public IReadOnlyCollection<Location> TSPSolution { get { return ClusterTSPRoute.Schedule; } }

            public ContinuousCluster(double x, double y, ILocationsProvider locationsProvider, IDistancesProvider distancesProvider)
            {
                X = x;
                Y = y;
                ClusterTSPRoute = new NoFurtherOptRoute(locationsProvider.GetDepot(), locationsProvider.GetDepot(), distancesProvider);
            }

            public void Add(Location location)
            {
                ClusterTSPRoute.TryInsert(location, out double insertionCost);
            }
        }

        protected List<Location> locationStore;

        protected int kClustersPerVehicle;
        protected List<VRP.Core.VRPAlgorithms.VRPAlgorithm> heuristicOptimizers;
        protected int iterations;
        protected int swarmSize;
        protected Dictionary<int, Location> frozenLocations;

        protected bool Freeze { get; set; }
        protected int Ratio { get; set; }

        public override string Name => string.Format("{0}{1}-{2}-{3}", "ContinuousClustering", Type, Freeze, Ratio);

        protected abstract string Type { get; }

        public double InitializationRadius => Math.Max(GetXSpread(),
            GetYSpread());

        private double GetYSpread()
        {
            return locationStore.Max(loc => loc.Y) - locationStore.Min(loc => loc.Y);
        }

        private double GetXSpread()
        {
            return locationStore.Max(loc => loc.X) - locationStore.Min(loc => loc.X);
        }

        public int Dimension { get; private set; }

        public IAllDataProvider AllProvider { get; protected set; }

        public override bool Deterministic => false;

        public BaseContinuousVRP(int swarmSize, int iterations, int kClustersPerVehicle, List<VRP.Core.VRPAlgorithms.VRPAlgorithm> heuristicOptimizers, IAllDataProvider provider) : base(provider, provider, provider)
        {
            this.swarmSize = swarmSize;
            this.iterations = iterations;
            this.kClustersPerVehicle = kClustersPerVehicle;
            this.heuristicOptimizers = heuristicOptimizers;
            this.AllProvider = provider;
            this.frozenLocations = new Dictionary<int, Location>();
            heuristicOptimizers.ForEach(opt => opt.Optimize());
            Dimension = 2 * kClustersPerVehicle * heuristicOptimizers.Max(opt => opt.GetSolution().Count);
            locationStore = provider.GetLocations();
        }

        protected BaseContinuousVRP(int swarmSize, int iterations, int kClustersPerVehicle, List<VRP.Core.VRPAlgorithms.VRPAlgorithm> heuristicOptimizers, IAllDataProvider provider, Dictionary<int, Location> frozenLocations) :
            this(swarmSize, iterations, kClustersPerVehicle, heuristicOptimizers, provider)
        {
            this.frozenLocations = frozenLocations;
        }


        public void AdaptFunctionStates(List<IFunction> function)
        {
            if (function.Min(fnc => fnc.Dimension) != function.Max(fnc => fnc.Dimension))
                throw new VRP.Exceptions.NoSolutionException("Solutions differ in size");
        }

        public double CreateHistoricSolution()
        {
            this.Vehicles.Clear();
            this.Vehicles.AddRange(this.heuristicOptimizers[1].GetSolution());
            return this.TotalLength;
        }

        public double CreateInitialSolution()
        {
            this.Vehicles.Clear();
            this.Vehicles.AddRange(this.heuristicOptimizers[0].GetSolution());
            return this.TotalLength;
        }

        public double[] GetContinuousRepresentation()
        {
            double[] solution = new double[Dimension];
            for (int i = 0; i < Dimension; i += 8)
            {
                if (i + 1 < Dimension)
                {
                    solution[i] = locationStore.Max(loc => loc.X) + Common.Utils.RANDOM.NextDouble() * GetXSpread();
                    solution[i + 1] = locationStore.Max(loc => loc.Y) + Common.Utils.RANDOM.NextDouble() * GetYSpread();
                }
                if (i + 3 < Dimension)
                {
                    solution[i + 2] = locationStore.Min(loc => loc.X) - Common.Utils.RANDOM.NextDouble() * GetXSpread();
                    solution[i + 3] = locationStore.Max(loc => loc.Y) + Common.Utils.RANDOM.NextDouble() * GetYSpread();
                }
                if (i + 5 < Dimension)
                {
                    solution[i + 4] = locationStore.Min(loc => loc.X) - Common.Utils.RANDOM.NextDouble() * GetXSpread();
                    solution[i + 5] = locationStore.Min(loc => loc.Y) - Common.Utils.RANDOM.NextDouble() * GetYSpread();
                }
                if (i + 7 < Dimension)
                {
                    solution[i + 6] = locationStore.Max(loc => loc.X) + Common.Utils.RANDOM.NextDouble() * GetXSpread();
                    solution[i + 7] = locationStore.Min(loc => loc.Y) - Common.Utils.RANDOM.NextDouble() * GetYSpread();
                }
            }
            for (int clustIndex = 0; clustIndex < kClustersPerVehicle; ++clustIndex)
            {
                List<Vehicle> solutionVehicles = this.GetSolution();
                for (int vehId = 0; vehId < solutionVehicles.Count && clustIndex * solutionVehicles.Count + 2 * vehId + 1 < solution.Length; ++vehId)
                {
                    var vehiclesLocations = solutionVehicles[vehId].Schedule.Where(loc => loc.Size > 0);
                    if (vehiclesLocations.Count() >= kClustersPerVehicle)
                    {
                        var locationsPerCluster = vehiclesLocations.Count() / kClustersPerVehicle;
                        solution[2 * clustIndex * solutionVehicles.Count + 2 * vehId + 0] =
                            vehiclesLocations.Skip(clustIndex * locationsPerCluster).Take(locationsPerCluster).Average(loc => loc.X) +
                            (Common.Utils.RANDOM.NextDouble() - 0.5) * 0.001 * GetXSpread();
                        solution[2 * clustIndex * solutionVehicles.Count + 2 * vehId + 1] =
                            vehiclesLocations.Skip(clustIndex * locationsPerCluster).Take(locationsPerCluster).Average(loc => loc.Y) +
                            (Common.Utils.RANDOM.NextDouble() - 0.5) * 0.001 * GetYSpread();

                    }
                    else
                    {
                        solution[2 * clustIndex * solutionVehicles.Count + 2 * vehId + 0] = vehiclesLocations.Average(loc => loc.X) +
                            (Common.Utils.RANDOM.NextDouble() - 0.5) * 0.001 * GetXSpread();
                        solution[2 * clustIndex * solutionVehicles.Count + 2 * vehId + 1] = vehiclesLocations.Average(loc => loc.Y) +
                            (Common.Utils.RANDOM.NextDouble() - 0.5) * 0.001 * GetYSpread();
                    }
                }
            }
            return solution;
        }

        public double[] GetRandomSolution()
        {
            double[] solution = new double[Dimension];
            for (int i = 0; i < Dimension; i += 2)
            {
                solution[i] = locationStore.Min(loc => loc.X) + Common.Utils.RANDOM.NextDouble() * GetXSpread();
                solution[i + 1] = locationStore.Min(loc => loc.Y) + Common.Utils.RANDOM.NextDouble() * GetYSpread();
            }
            return solution;
        }

        public override void Optimize()
        {
            Freeze = true;
            Ratio = 2;
            var optimAlgorithm = new PSOAlgorithm(
                function: this,
                swarmSize: swarmSize,
                radius: this.InitializationRadius,
                neighbourhood: 0.5,
                inventorsRatio: 0.5);
            int i = 0;
            for (; i < iterations / Ratio + 1; ++i)
            {
                optimAlgorithm.Step();
            }
            this.Value(optimAlgorithm.Best);
            Vehicles.ForEach(
                vhcl => vhcl.Optimize());
            double initialBestValue = this.TotalLength;
            double[] initialBest = optimAlgorithm.Best;
            if (Freeze)
            {
                FreezeLocations();
                optimAlgorithm.ReloadState(this);
            }
            for (; i < iterations; ++i)
            {
                optimAlgorithm.Step();
            }
            this.Value(optimAlgorithm.Best);
            Vehicles.ForEach(
                vhcl => vhcl.Optimize());
            if (this.TotalLength > initialBestValue)
            {
                this.DefrostLocations();
                this.Value(initialBest);
            }
            if (this.TotalLength > heuristicOptimizers[0].TotalLength)
            {
                this.ResetVehicles();
                this.Vehicles.AddRange(heuristicOptimizers[0].Vehicles);
            }
            if (this.TotalLength > heuristicOptimizers[1].TotalLength)
            {
                this.ResetVehicles();
                this.Vehicles.AddRange(heuristicOptimizers[1].Vehicles);
            }
        }

        public void FreezeLocations()
        {
            //TODO: first added to the cluster should be the one frozen - it sets the whole solution
            frozenLocations = this.Vehicles
                .Take(Dimension / 2 / kClustersPerVehicle)
                .Select((veh, idx) =>
                    new KeyValuePair<int, Location>(idx, veh.Schedule.Count > 2 ? veh.Schedule.ElementAt(1) : null))
                .Where(pair => pair.Value != null)
                .ToDictionary(pair =>
                    pair.Key,
                    pair => pair.Value);
            /*
            //alternative - with size
            frozenLocations = this.Vehicles
                .Select((veh, idx) =>
                    new KeyValuePair<int, Location>(idx, veh.Schedule.Count > 2 ? 
                        veh.Schedule.First(loc1 => veh.Schedule.Max(loc => loc.Size) == loc1.Size)
                        : null))
                .Where(pair => pair.Value != null)
                .ToDictionary(pair =>
                    pair.Key,
                    pair => pair.Value);
            */
        }

        public void DefrostLocations()
        {
            frozenLocations.Clear();
        }

        private List<ContinuousCluster> ConstructClusters(double[] x)
        {
            List<ContinuousCluster> clusters = new List<ContinuousCluster>();
            for (int i = 0; i < Dimension; i += 2)
            {
                clusters.Add(new ContinuousCluster(x[i], x[i + 1], LocationsProvider, DistancesProvider));
            }

            return clusters;
        }

        public double Value(double[] x)
        {
            try
            {
                List<ContinuousCluster> clusters = ConstructClusters(x);
                var locationsToCluster = LocationsProvider.GetLocations();
                ClusterLocations(out List<Location> unassignedLocations, clusters, locationsToCluster);
                ResetVehicles();
                CreateSolutionFromClusters(clusters, unassignedLocations);
                return this.TotalLength;
            }
            catch (Exception ex)
            {
                Console.Error.Write(ex.StackTrace);
                return double.MaxValue;
            }
        }

        protected abstract void ClusterLocations(out List<Location> unassignedLocations, List<ContinuousCluster> clusters, List<Location> locationsToCluster);

        protected void MakeFeasibleSolution(List<Location> unassignedLocations, int activeVehiclesCount)
        {
            var orderedVehicles = Vehicles
                .OrderByDescending(veh => veh.Schedule.Count)
                .ToList();
            var depot = LocationsProvider.GetDepot();
            var orderedLocations = unassignedLocations
                .OrderByDescending(loc => DistancesProvider.GetDistance(loc, depot))
                .ToList();
            foreach (Location loc in orderedLocations)
            {
                double cheapestInsertion = double.PositiveInfinity;
                Vehicle selectedVehicle = null;
                Location selectedLocation = null;
                GetChepestVehicleForLocation(orderedVehicles, ref cheapestInsertion, ref selectedLocation, ref selectedVehicle, loc);
                if (selectedVehicle != null)
                    selectedVehicle.Insert(loc);
                else
                    throw new InfeasibleSolutionException("Cannot repair solution");
            }
        }

        private void CreateSolutionFromClusters(List<ContinuousCluster> clusters, List<Location> unassignedLocations)
        {
            int activeVehiclesCount = clusters.Count / kClustersPerVehicle;
            for (int i = 0; i < clusters.Count; i++)
            {
                var locations =
                    clusters[i]
                    .TSPSolution
                    .Where(loc => loc.Size > 0);
                foreach (var location in locations)
                {
                    location.ClearRoute();
                    if (!Vehicles[i % activeVehiclesCount].Add(location))
                        unassignedLocations.Add(location);
                }
            }
            MakeFeasibleSolution(unassignedLocations, activeVehiclesCount);
        }

        protected void ResetVehicles()
        {
            Vehicles.Clear();
            Vehicles.AddRange(VehiclesProvider.GetVehicles());
        }

        public abstract IFunction CreateCopy();
    }
}
