﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ParticleSwarmOptimization;
using VRP.Core;
using VRP.Providers;
using VRP.Core.Locations;

namespace ContVRP
{
    public class ContinuousVRPWithExplicitCapacityClustering : BaseContinuousVRP
    {
        protected override string Type => "V2";

        public ContinuousVRPWithExplicitCapacityClustering(int swarmSize, int iterations, int kClustersPerVehicle, List<VRP.Core.VRPAlgorithms.VRPAlgorithm> heuristicOptimizers, IAllDataProvider provider)
            : base(swarmSize, iterations, kClustersPerVehicle, heuristicOptimizers, provider)
        {
        }

        protected ContinuousVRPWithExplicitCapacityClustering(int swarmSize, int iterations, int kClustersPerVehicle, List<VRP.Core.VRPAlgorithms.VRPAlgorithm> heuristicOptimizers, IAllDataProvider provider, Dictionary<int, Location> frozenLocations):
            base(swarmSize, iterations, kClustersPerVehicle, heuristicOptimizers, provider, frozenLocations)
        {
        }


        protected override void ClusterLocations(out List<Location> unassignedLocations, List<ContinuousCluster> clusters, List<Location> locationsToCluster)
        {
            Dictionary<ContinuousCluster, List<ContinuousCluster>> clusterGroups;
            Dictionary<List<ContinuousCluster>, double> clusterGropusSums;
            PrepareClusterGroups(clusters, locationsToCluster, out clusterGroups, out clusterGropusSums);
            double capacity;
            IOrderedEnumerable<Location> orderedLocationsToCluster;
            IOrderedEnumerable<ContinuousCluster> activeClusters;
            InitializeObjectsForClusterLocations(out unassignedLocations, clusters, locationsToCluster, out capacity, out orderedLocationsToCluster, out activeClusters);
            foreach (Location location in orderedLocationsToCluster)
            {
                double minDistance = double.PositiveInfinity;
                ContinuousCluster selectedCluster = null;
                foreach (ContinuousCluster cluster in activeClusters)
                {
                    double testDistance = (cluster.X - location.X) * (cluster.X - location.X) +
                        (cluster.Y - location.Y) * (cluster.Y - location.Y);
                    SelectionRule(clusterGroups, clusterGropusSums, capacity, location, ref minDistance, ref selectedCluster, cluster, testDistance);
                }
                InsertLocationIntoSelectedCluster(unassignedLocations, clusterGroups, clusterGropusSums, location, selectedCluster);
            }
        }

        protected static void SelectionRule(Dictionary<ContinuousCluster, List<ContinuousCluster>> clusterGroups, Dictionary<List<ContinuousCluster>, double> clusterGropusSums, double capacity, Location location, ref double minDistance, ref ContinuousCluster selectedCluster, ContinuousCluster cluster, double testDistance)
        {
            if (testDistance < minDistance && clusterGropusSums[clusterGroups[cluster]] + location.Size <= capacity)
            {
                selectedCluster = cluster;
                minDistance = testDistance;
            }
        }

        protected static void InsertLocationIntoSelectedCluster(List<Location> unassignedLocations, Dictionary<ContinuousCluster, List<ContinuousCluster>> clusterGroups, Dictionary<List<ContinuousCluster>, double> clusterGropusSums, Location location, ContinuousCluster selectedCluster)
        {
            if (selectedCluster != null)
            {
                selectedCluster.Add(location);
                clusterGropusSums[clusterGroups[selectedCluster]] += location.Size;
            }
            else
                unassignedLocations.Add(location);
        }

        protected void InitializeObjectsForClusterLocations(out List<Location> unassignedLocations, List<ContinuousCluster> clusters, List<Location> locationsToCluster, out double capacity, out IOrderedEnumerable<Location> orderedLocationsToCluster, out IOrderedEnumerable<ContinuousCluster> activeClusters)
        {
            unassignedLocations = new List<Location>();
            capacity = AllProvider.GetCapacities()[0];
            var depot = AllProvider.GetDepot();
            orderedLocationsToCluster = locationsToCluster.OrderByDescending(loc => AllProvider.GetDistance(loc, depot));
            activeClusters = clusters.OrderByDescending(cl => (cl.X - depot.X) * (cl.X - depot.X)
+ (cl.Y - depot.Y) * (cl.Y - depot.Y));
        }

        protected void PrepareClusterGroups(List<ContinuousCluster> clusters, List<Location> locationsToCluster, out Dictionary<ContinuousCluster, List<ContinuousCluster>> clusterGroups, out Dictionary<List<ContinuousCluster>, double> clusterGropusSums)
        {
            clusterGroups = new Dictionary<ContinuousCluster, List<ContinuousCluster>>();
            clusterGropusSums = new Dictionary<List<ContinuousCluster>, double>();
            int consideredVehicles = clusters.Count / kClustersPerVehicle;
            for (int i = 0; i < consideredVehicles; i++)
            {
                var clusterGroup = new List<ContinuousCluster>();
                clusterGropusSums.Add(clusterGroup, 0.0);
                for (int j = 0; j < kClustersPerVehicle; j++)
                {
                    clusterGroup.Add(clusters[i + j * consideredVehicles]);
                    clusterGroups.Add(clusters[i + j * consideredVehicles], clusterGroup);
                }
            }
            foreach (KeyValuePair<int, Location> idedLocation in frozenLocations)
            {
                if (locationsToCluster.RemoveAll(loc => idedLocation.Value.Id == loc.Id) > 0)
                {
                    //HACK: identical to typical assignment while clustering
                    idedLocation.Value.ClearRoute();
                    clusters[idedLocation.Key].Add(idedLocation.Value);
                    clusterGropusSums[clusterGroups[clusters[idedLocation.Key]]] += idedLocation.Value.Size;
                }
            }
        }

        public override IFunction CreateCopy()
        {
            return new ContinuousVRPWithExplicitCapacityClustering(this.swarmSize, this.iterations, this.kClustersPerVehicle, this.heuristicOptimizers, AllProvider, frozenLocations);
        }


    }
}
