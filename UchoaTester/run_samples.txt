dotnet run -c Release -i 1000 -s 20 -k 1 >contvrp2.i1000.s20.k1.csv
dotnet run -c Release -i 1000 -s 20 -k 1 -r 10 >contvrp2.i1000.s20.k1.r10.csv
dotnet run -c Release -i 200 -s 20 -k 1 -r 5 >contvrp2.i200.s20.k1.r5.csv
dotnet run -c Release -i 400 -s 40 -k 1 -r 10 --skip 83 >>contvrp2.i400.s40.k1.r10.csv
dotnet run -c Release -i 400 -s 40 -k 1 -r 10 --skip 92 >>contvrp2.i400.s40.k1.r10.2.csv
dotnet run -c Release -i 400 -s 40 -k 1 -r 10 --skip 96 >>contvrp2.i400.s40.k1.r10.3.csv
dotnet run -c Release -i 400 -s 40 -k 1 -r 10 --skip 97 >>contvrp2.i400.s40.k1.r10.97.csv
dotnet run -c Release -i 400 -s 40 -k 1 -r 10 --skip 98 >>contvrp2.i400.s40.k1.r10.98.csv
dotnet run -c Release -i 400 -s 40 -k 1 -r 10 --skip 99 >>contvrp2.i400.s40.k1.r10.99.csv
