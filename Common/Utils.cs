﻿using System;

namespace Common
{
    public class Utils
    {
        public static readonly Random RANDOM = new Random();

        public static double GetDistanceBewteenCoordinates(double lat1, double lon1, double lat2, double lon2)
        {
            double r = 6371.0;
            double dLat = Math.Abs(lat2 - lat1) * 0.01745329251994329576923690768489;
            double dLon = Math.Abs(lon2 - lon1) * 0.01745329251994329576923690768489;
            double lat1Rad = lat1 * 0.01745329251994329576923690768489;
            double lat2Rad = lat2 * 0.01745329251994329576923690768489;

            double a = Math.Sin(dLat / 2.0) * Math.Sin(dLat / 2.0) + Math.Sin(dLon / 2.0) * Math.Sin(dLon / 2.0) * Math.Cos(lat1Rad) * Math.Cos(lat2Rad);
            double c = 2.0 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1.0 - a));
            double d = r * c;

            return 1000.0 * d;
        }
    }
}
