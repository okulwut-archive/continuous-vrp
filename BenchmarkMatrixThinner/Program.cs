﻿using CommandLine;
using CommandLine.Text;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using VRP.Core.TSPAlgorithms;
using VRP.Providers;
using VRP.Providers.VRPLIB;
using VRP.Utils.Clustering;

namespace BenchmarkMatrixThinner
{
    class Program
    {
        public class Options
        {
            [Option('t', "thickness",
                Default = 10,
                HelpText = "Number of spanning trees.")]
            public int Iterations { get; set; }
            [Option('b', "benchmarks",
                Default = "../VRP/Instances/Warsaw",
                HelpText = "Location with benchmarks to process")]
            public string BenchmarksDirectory { get; set; }

        }


        private static Options ParseRuntimeOptions(string[] args)
        {
            ParserResult<Options> parsed = Parser.Default.ParseArguments<Options>(args);
            if (parsed.Tag == ParserResultType.NotParsed)
            {
                NotParsed<Options> notparsed = (CommandLine.NotParsed<Options>)parsed;
                if (!notparsed.Errors.Any(err => err.Tag == ErrorType.VersionRequestedError || err.Tag == ErrorType.HelpRequestedError))
                {
                    /*
                    foreach (var error in
                        notparsed
                        .Errors
                        .Where(err =>
                            err.Tag != ErrorType.VersionRequestedError &&
                            err.Tag != ErrorType.HelpRequestedError))
                    {
                        var info = (error is MissingRequiredOptionError) ?
                            (error as MissingRequiredOptionError).NameInfo.LongName : "";
                    logger.LogCritical($"{error.Tag} {info}");
                    }
                    */
                }
                if (notparsed.Errors.Any(err => err.StopsProcessing || err.Tag == ErrorType.MissingRequiredOptionError))
                {
                    Environment.Exit(1);
                }
            }
            Options options = (Options)parsed.MapResult(
                (Options opt) => opt,
                _ => Nothing());
            return options;
        }

        private static object Nothing()
        {
            return null;
        }

        static void Main(string[] args)
        {
            Options options = ParseRuntimeOptions(args);
            var fileList = Directory.EnumerateFiles(options.BenchmarksDirectory);
            var fileInfos = fileList
                .Select(fn => new FileInfo(fn))
                .Where(fi => fi.Extension.EndsWith("vrp"))
                ;
            foreach (var fileInfo in fileInfos)
            {
                IAllDataProvider provider = ProviderFactory.GetProviderForFile(fileInfo, new NoFurtherOptRouteFactory(), false);
                if (provider == null)
                    continue;
                var locations = provider.GetLocations();
                List<ClusteredLocation> clusteredLocations = new List<ClusteredLocation>();
                locations.ForEach(
                    loc => clusteredLocations.Add(new ClusteredLocation(loc)));
                var edges = GraphMSTClusterer.ComputeOrderedEdges(provider, clusteredLocations, clusteredLocations);
                var outputEdges = new List<Edge>();
                for (int i = 0; i < options.Iterations; i++)
                {
                    var clusters = new GraphMSTClusterer(double.MaxValue, clusteredLocations, edges);
                    var treeEdges = clusters.EdgesOutput;
                    outputEdges.AddRange(treeEdges);
                    var tempEdges = edges.ToList();
                    foreach (Edge e in treeEdges)
                        tempEdges.Remove(e);
                    edges = tempEdges.OrderBy(e => e.distance);
                }
                var wrappedDepot = new List<ClusteredLocation>() { new ClusteredLocation(provider.GetDepot()) };
                outputEdges.AddRange(GraphMSTClusterer.ComputeOrderedEdges(provider, clusteredLocations, wrappedDepot));
                outputEdges.AddRange(GraphMSTClusterer.ComputeOrderedEdges(provider, wrappedDepot, clusteredLocations));
                var lines = outputEdges.Select(edge => string.Format("{0} {1} {2}", edge.locationFrom.Id, edge.locationTo.Id, edge.distance));
                File.WriteAllLines(
                    options.BenchmarksDirectory + "/" + fileInfo.Name.Replace(fileInfo.Extension, ".matrix"),
                    lines);
                //also edges to depot!
            }
        }
    }
}